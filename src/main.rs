use std::env::var;
use std::error::Error as StdError;
use std::mem::take;
use std::net::SocketAddr;
use std::str::from_utf8;

use blake3::{Hash, hash};
use bytes::{Bytes, BytesMut};
use futures_util::stream::{StreamExt, try_unfold};
use hashlink::LinkedHashMap;
use http::uri::{Authority, PathAndQuery, Scheme};
use http_body_util::{BodyExt, Either, Empty, StreamBody};
use hyper::{
    Method, Request, Response, StatusCode, Uri,
    body::{Body, Frame, Incoming},
    header::{CONTENT_LENGTH, CONTENT_TYPE},
    server::conn::http1::Builder,
    service::service_fn,
};
use hyper_util::{
    client::legacy::{Client, connect::HttpConnector},
    rt::{TokioExecutor, TokioIo, TokioTimer},
};
use serde::Deserialize;
use serde_roxmltree::from_str;
use tokio::{
    fs::{File, metadata, read_dir, remove_file},
    io::{AsyncReadExt, AsyncWriteExt, BufWriter},
    net::TcpListener,
    sync::{
        Mutex,
        watch::{Receiver, Sender, channel},
    },
    task::spawn,
};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Fallible {
    let bind_addr = var("BIND_ADDR")?.parse::<SocketAddr>()?;
    let stor_lim = var("STOR_LIM")?.parse::<u64>()?;

    let mut stor_sum = 0;
    let mut files = LinkedHashMap::new();

    let mut dir = read_dir(".").await?;

    while let Some(entry) = dir.next_entry().await? {
        if let Ok(file_name) = entry.file_name().into_string() {
            if let Ok(id) = Hash::from_hex(&file_name) {
                stor_sum += entry.metadata().await?.len();

                let (_, recv) = channel(());
                files.insert(id, recv);
            }
        }
    }

    let state = &*Box::leak(Box::new(State {
        client: Client::builder(TokioExecutor::new()).build_http(),
        inner: Mutex::new(StateInner {
            stor_lim,
            stor_sum,
            files,
        }),
    }));

    let listener = TcpListener::bind(bind_addr).await?;

    loop {
        let (socket, _) = listener.accept().await?;

        spawn(async move {
            if let Err(err) = Builder::new()
                .timer(TokioTimer::new())
                .serve_connection(
                    TokioIo::new(socket),
                    service_fn(move |req| async move {
                        match handle_request(state, req).await {
                            Ok(resp) => Ok(resp.map(Either::Left)),
                            Err(err) => {
                                let msg = format!("Failed to handle request: {err}");

                                eprintln!("{msg}");

                                Response::builder()
                                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                                    .header(CONTENT_TYPE, "text/plain")
                                    .body(Either::Right(msg))
                            }
                        }
                    }),
                )
                .await
            {
                eprintln!("Unhandled internal error: {err}");
            }
        });
    }
}

struct State {
    client: Client<HttpConnector, Empty<Bytes>>,
    inner: Mutex<StateInner>,
}

struct StateInner {
    stor_lim: u64,
    stor_sum: u64,
    files: LinkedHashMap<Hash, Receiver<()>>,
}

async fn handle_request(
    state: &'static State,
    mut req: Request<Incoming>,
) -> Fallible<Response<Either<impl Body<Data = Bytes, Error = Error>, Incoming>>> {
    if req.method() == Method::GET && req.uri().path().ends_with(".rpm") {
        return handle_package_request(state, req).await;
    }

    println!("Fetching: {}", req.uri());
    change_request_target(req.uri_mut())?;
    let resp = state.client.request(req.map(|_body| Empty::new())).await?;

    Ok(resp.map(Either::Right))
}

async fn handle_package_request(
    state: &'static State,
    mut req: Request<Incoming>,
) -> Fallible<Response<Either<impl Body<Data = Bytes, Error = Error>, Incoming>>> {
    let id = hash(req.uri().path().as_bytes());

    let mut inner = state.inner.lock().await;

    let body = if let Some(recv) = inner.files.get(&id) {
        let mut recv = recv.clone();

        println!("Cached package: {}", req.uri());
        let file = File::open(id.to_hex().as_str()).await?;

        drop(inner);

        recv.changed().await.unwrap_err();

        struct Context {
            file: File,
            buf: BytesMut,
        }

        let ctx = Context {
            file,
            buf: BytesMut::with_capacity(BUF_CAP),
        };

        try_unfold(ctx, |mut ctx| async {
            if ctx.buf.capacity() == 0 {
                ctx.buf.reserve(BUF_CAP);
            }

            match ctx.file.read_buf(&mut ctx.buf).await {
                Ok(0) => Ok(None),
                Ok(_) => {
                    let buf = ctx.buf.split().freeze();

                    Ok(Some((Frame::data(buf), ctx)))
                }
                Err(err) => Fallible::Err(err.into()),
            }
        })
        .boxed()
    } else {
        while inner.stor_lim < inner.stor_sum {
            let id = match inner.files.pop_front() {
                Some((id, _)) => id,
                _ => break,
            };

            inner.stor_sum -= metadata(id.to_hex().as_str()).await?.len();

            println!("Removing stale package: {id}");
            remove_file(id.to_hex().as_str()).await?;
        }

        println!("Fetching package: {}", req.uri());
        change_request_target(req.uri_mut())?;
        let resp = state.client.request(req.map(|_body| Empty::new())).await?;

        if !resp.status().is_success() {
            return Ok(resp.map(Either::Right));
        }

        let (resp, size) = match resp.headers().get(CONTENT_TYPE) {
            Some(content_type) if content_type.to_str()?.contains("application/metalink4+xml") => {
                let metalink = from_str::<Metalink>(from_utf8(
                    &resp.into_body().collect().await?.to_bytes(),
                )?)?;

                let size = metalink.file.size.unwrap_or(0);
                let uri = Uri::try_from(metalink.url().ok_or("Metalink without URL")?)?;

                println!("Resolved metalink: {uri}");

                let req = Request::builder()
                    .method(Method::GET)
                    .uri(uri)
                    .body(Empty::new())
                    .unwrap();

                let resp = state.client.request(req).await?;

                if !resp.status().is_success() {
                    return Ok(resp.map(Either::Right));
                }

                (resp, size)
            }
            _ => {
                let size = match resp.headers().get(CONTENT_LENGTH) {
                    Some(content_length) => content_length.to_str()?.parse()?,
                    None => 0,
                };

                (resp, size)
            }
        };

        let file = File::create(id.to_hex().as_str()).await?;
        file.set_len(size).await?;

        let (send, recv) = channel(());
        inner.files.insert(id, recv);

        drop(inner);

        struct Context {
            state: &'static State,
            _send: Sender<()>,
            id: Hash,
            writer: BufWriter<File>,
            body: Incoming,
            len: usize,
            ok: bool,
        }

        impl Drop for Context {
            fn drop(&mut self) {
                if self.ok {
                    return;
                }

                let state = self.state;
                let id = self.id;

                spawn(async move {
                    let mut inner = state.inner.lock().await;
                    inner.files.remove(&id);

                    eprintln!("Removing incomplete package: {id}");
                    let _ = remove_file(id.to_hex().as_str()).await;
                });
            }
        }

        let ctx = Context {
            state,
            _send: send,
            id,
            writer: BufWriter::with_capacity(BUF_CAP, file),
            body: resp.into_body(),
            len: 0,
            ok: false,
        };

        try_unfold(ctx, move |mut ctx| async {
            match ctx.body.frame().await {
                None => {
                    ctx.writer.flush().await?;

                    let mut inner = ctx.state.inner.lock().await;
                    inner.stor_sum += ctx.len as u64;

                    ctx.ok = true;

                    Ok(None)
                }
                Some(Ok(frame)) => {
                    let buf = frame.data_ref().unwrap();

                    ctx.writer.write_all(buf).await?;
                    ctx.len += buf.len();

                    Ok(Some((frame, ctx)))
                }
                Some(Err(err)) => Fallible::Err(err.into()),
            }
        })
        .boxed()
    };

    let resp = Response::builder()
        .header(CONTENT_TYPE, "application/octet-stream")
        .body(Either::Left(StreamBody::new(body)))
        .unwrap();

    Ok(resp)
}

fn change_request_target(uri: &mut Uri) -> Fallible {
    let mut parts = take(uri).into_parts();

    let path = parts
        .path_and_query
        .as_ref()
        .ok_or("URI without path")?
        .path();

    let mut comps = path.match_indices('/');
    let host_start = comps.next().ok_or("Path without host")?.0;
    let host_end = comps.next().ok_or("Path without host")?.0;
    if host_start >= host_end {
        return Err("Path without host".into());
    }

    let host = &path[host_start + 1..host_end];
    let path = &path[host_end..];

    parts.scheme = Some(Scheme::HTTP);
    parts.authority = Some(Authority::try_from(host)?);
    parts.path_and_query = Some(PathAndQuery::try_from(path)?);

    *uri = Uri::from_parts(parts).unwrap();

    Ok(())
}

#[derive(Deserialize)]
struct Metalink {
    file: MetalinkFile,
}

impl Metalink {
    fn url(self) -> Option<String> {
        self.file
            .url
            .into_iter()
            .min_by_key(|url| url.priority)
            .map(|url| url.value)
    }
}

#[derive(Deserialize)]
struct MetalinkFile {
    size: Option<u64>,
    url: Vec<MetalinkUrl>,
}

#[derive(Deserialize)]
struct MetalinkUrl {
    priority: u32,
    #[serde(rename = "#content")]
    value: String,
}

const BUF_CAP: usize = 64 * 1024;

type Fallible<T = ()> = Result<T, Error>;

type Error = Box<dyn StdError + Send + Sync>;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn first_path_component_becomes_authority() {
        let mut uri =
            Uri::from_static("http://localhost/download.opensuse.org/tumbleweed/repo/oss/");
        change_request_target(&mut uri).unwrap();
        assert_eq!(
            uri,
            Uri::from_static("http://download.opensuse.org/tumbleweed/repo/oss/")
        );

        let mut uri = Uri::from_static(
            "http://localhost/ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/Essentials",
        );
        change_request_target(&mut uri).unwrap();
        assert_eq!(
            uri,
            Uri::from_static(
                "http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/Essentials"
            )
        );
    }
}
